#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# librerias estandar
import time

#-----------------------------------------------------------------------------#
import rtmidi


class MIDI_OUT(object):

    """
La Clase MIDI_OUT implementa los parámetros  necesarios para poder enviar
señales midi a un dispositivo de software (por ejemplo LMMS en sistemas Linux,
o ABLETON-LIVE en Windows y Mac).
Su principal objetivo es ser una clase sencilla para poder iniciar y ejecutar
notas mediante PYTHON, por eso cuenta con los parámetros principales

cargar_puerto(): crea un diccionario con cada dispositivo conectado y activo.
Imprime en pantalla los nombres de los dispositivos encontrados.

iniciar(puerto): recibe un string con el nombre del dispositivo a activar y
lo inicia.

La clase MIDI_OUT una vez instanciada como objeto puede generar notas y
acordes y tiene las siguientes variables:

bpm = valor del tiempo de espera de cada nota/acorde (por defecto 120)
volumen = volumen general de las notas/acordes (por defecto 100)

para generar sonidos se usan los siguientes metodos:
notas(valor,tiempo) :
 |
  - - > valor: (0 - 128) representa la nota midi a enviar
 |
- - > tiempo: la duración en BPM de la nota, por ejemplo si tiempo = 1, y
      BPM = 120, la duración de la nota sera 0.5 segundos

acorde(lista , tiempo)
 |
   - - > lista : es una lista de valores [n1,2,n3….] que contiene al menos un
   valor que sera representado como una nota (0 – 128).

-----------------------------------------------------------------------------–
nota() y acorde() son implementaciones sencillas para generar con la clase
MIDI_OUT, sin embargo, tienen limitaciones en su uso dado que funcionan
una a la vez, si se ejecuta acorde, hasta que termine de sonar, no se puede
hacer ninguna otra operación (como llamar a otras notas o mandar señales a
dispositivos físicos). Estas implementaciones funcionan como metodos sencillos
para aprender a usar la clase MIDI_OUT, que además es una abstracción de
rtmidi.
Para poder trabajar con la potencialidad de portmidi, se usan los metodos:

note_on(nota, volumen): enviá una señal de activación de notas
|
 - - > nota: enviá el valor 0 – 128 al puerto midi
|
- - > volumen: enviá el valor que representa el volumen de la nota midi


note_off(nota, volumen): enviá una señal para apagar la nota especificada
|
 - - > nota: enviá el valor 0 – 128 al puerto midi
|
- - > volumen: enviá el valor que representa el volumen de la nota midi


con note_on() podemos activar una nota MIDI, sin embargo, seguira sonando
hasta que enviemos una señal con note_off() a la misma nota musical. Esto nos
permitiría activar las notas que deseemos (como un acorde) y
de múltiples dispositivos a la vez.

Ejemplo basico de implementar MIDI_OUT:

import tomate

tp = tomate.MIDI_OUT()
tp.cargar_puertos()
tp.iniciar("LMMS:TripleOscillator 129:0")
tp.volumen=10
tp.bpm=120
for a in range(1,8):
     tp.acorde([70,73,76],1)
     tp.nota(70+a,1)
    """

    def __init__(self):
        """TODO: to be defined. """
        self.out = rtmidi.MidiOut()
        self.puertos = {}
        self.bpm = 120
        self.volumen = 100
        self.canal = 0

    def iniciar(self, puerto):
        """inicia el puerto midi como dispositivio de salida

        :puerto: String con el nombre del puerto previamente
        cargado con cargar_puerto
        :returns: TRue

        """
        try:
            self.out.open_port(self.puertos[puerto])
            print("inicio correcto")
            return True
        except Exception as err:
            print("ocurrio un error")
            raise err

    def cargar_puertos(self):
        """carga los puerto reconocidos en un diccionario y
        retorna el valor del dict
        :returns: diccionario con los valores del puerto

        """
        puertos_encontrados = self.out.get_ports()
        for puert in enumerate(puertos_encontrados):
            self.puertos[puert[1]] = puert[0]
        return self.puertos

    def __convertir_hex(self, nible1, nible2):
        """TODO: Docstring for convertir_hex.

        :a: TODO
        :returns: TODO

        """
        hexadecimal = "0x"
        dic_ff = {0: "0",
                  1: "1",
                  2: "2",
                  3: "3",
                  4: "4",
                  5: "5",
                  6: "6",
                  7: "7",
                  8: "8",
                  9: "9",
                  10: "A",
                  11: "B",
                  12: "C",
                  13: "D",
                  14: "E",
                  15: "F"}
        res = hexadecimal + dic_ff[nible1] + dic_ff[nible2]
        return res

    def note_on(self, nota, volumen=100):
        """envia una señal note on al dispositivo midi.
        :nota: (0 - 128) nota midi a enviar
        :volumen: (por defecto 100) volumen de la nota midi

        """
        primer_bit = self.__convertir_hex(9, self.canal)
        note_on = [int(primer_bit, 16), nota, volumen]
        self.out.send_message(note_on)
        print("nota: ", note_on)

    def note_off(self, nota, volumen=100):
        """Envia note off al dispositivo midi.
        :nota: (0 - 128) nota midi
        :volumen: (por defecto 100) volumen nota midi

        """
        primer_bit = self.__convertir_hex(8, self.canal)
        note_off = [int(primer_bit, 16), nota, volumen]
        self.out.send_message(note_off)
        print("nota: ", note_off)

    def tiempo(self, valor, bpm):
        """calcula tiempo en BPM para implementar pausas
        :valor: valor del tiempo a esperar (1 = una nota blanca, las
         subdivisiones representan las demas notas)
        :bpm: beats por minutos
        :returns: True
        """
        tiempo_final = ((1/(bpm/60))*valor)
        time.sleep(tiempo_final)
        print("espero : ", tiempo_final, " seg")
        return True

    def nota(self, valor_nt, tmp):
        """envia una nota al dispositivo midi y espera un tiempo de BPM
        :nt: Nota midi a enviar
        :tmp: subdivisión de tiempo a esperar en funcion al BPM
        (1 = una nota blanca, las subdivisiones representan las demas notas)
        """
        self.note_on(valor_nt, self.volumen)
        self.tiempo(tmp, self.bpm)
        self.note_off(valor_nt, self.volumen)

    def acorde(self, nts, tmp):
        """envia una lista con acordes para ejecutar en el dispositivo
        :nts: lista con valores [n1,n2,n3 ...] que representan las notas
        midi (0 - 128)
        :tmp: subdivisión de tiempo a esperar en funcion al BPM
        (1 = una nota blanca, las subdivisiones representan las demas notas)

        """
        for nota_a in nts:
            self.note_on(nota_a, self.volumen)
        self.tiempo(tmp, self.bpm)
        for nota_a_close in nts:
            self.note_off(nota_a_close, self.volumen)


class MIDI_IN(object):

    """Docstring for MIDI_IN. """

    def __init__(self):
        """TODO: to be defined. """
        self.m_in = rtmidi.MidiIn()
        self.puertos = {}
        self.bpm = 120
        self.volumen = 100
        self.canal = 0

    def iniciar(self, puerto):
        """inicia el puerto midi como dispositivio de salida

        :puerto: String con el nombre del puerto previamente
        cargado con cargar_puerto
        :returns: TRue

        """
        try:
            self.m_in.open_port(self.puertos[puerto])
            print("inicio correcto")
            return True
        except Exception as err:
            print("ocurrio un error")
            raise err

    def cargar_puertos(self):
        """carga los puerto reconocidos en un diccionario y
        retorna el valor del dict
        :returns: diccionario con los valores del puerto

        """
        puertos_encontrados = self.m_in.get_ports()
        for puert in enumerate(puertos_encontrados):
            self.puertos[puert[1]] = puert[0]
        return self.puertos

    def recibir(self):
        """TODO: Docstring for recibir.
        :returns: TODO

        """
        resultado = self.m_in.get_message()
        return resultado
