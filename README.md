                                                       
# Tomate

La librería TOMATE está diseñada para poder usar las entradas y salidas MIDI de forma sencilla para enseñar programación.
Es una librería inacabada, dado que parte de la propuesta es que los estudiantes puedan agregar funcionalidades y usarla como herramienta de desarrollo musical.

## Dependencias

Para instalar la lib y que funcione, es necesario instalar con pip:

pip install python-rtmidi


## Documentación

La Documentación se mantendrá en los Docstring de los archivos .py para que los estudiantes la tengan lo más a mano posible.
